module libmpcd.core.exception;

/**
 * A class to represent exceptions should be thrown when any API error occurs.
 */
class APIException : Exception
{
    /**
     * A constructor to create a new instance of APIException
     *
     * Params:
     *      msg = a message describing the error
     *      file = the file name of the source code corresponding with where
     *          the error was thrown from
     *      line = the line of the source code corresponding with where
     *          the error was thrown from
     *      next = a reference to the next error in the list
     */
    pure nothrow @nogc @safe this(string msg, string file = __FILE__,
        size_t line = __LINE__, Throwable next = null)
    {
        super(msg, file, line, next);
    }
}
