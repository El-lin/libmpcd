module libmpcd.core.connection;

/**
 * Structure to represent an abstraction over the current connection to
 * a MPD server.
 */
struct Connection
{
    import libmpcd.c.connection;

    @disable this();

    package(libmpcd)
    {
        mpd_connection* context;

        /**
         * A constructor.
         *
         * Params:
         *       context = a pointer to raw C connection, must not be null
         */
        this(mpd_connection* context)
        {
            this.context = context;
        }

        /**
         * Checks if the MPD command executed within the connection
         * was successful.
         *
         * Returns:
         *       a copy of itself
         *
         *  Throws:
         *       MPDException if there is any MPD error
         */
        typeof(this) checkError()
        {
            import libmpcd.mpderror;
            import std.string;
            import std.conv;
            import libmpcd.c.error : mpd_error;

            int error_code_int = mpd_connection_get_error(this.context);
            auto error_code = to!MPDErrorCode(error_code_int);

            if(error_code != MPDErrorCode.Success)
            {
                auto error_message =
                    fromStringz(mpd_connection_get_error_message(
                        this.context));
                throw new MPDException(error_code, error_message.idup);
            }
            return this;
        }

        /**
         * Ensures that last command executed within this MPD connection has
         * failed.
         *
         * Throws:
         *      MPDException if there is any MPD error and
         *      APIException if there is no error
         */
        void expectError()
        {
            import libmpcd.core.exception;

            this.checkError();
            throw new APIException("Expected fail, got success");
        }
    }

    invariant()
    {
        assert(this.context !is null);
    }
}
