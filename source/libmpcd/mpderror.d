module libmpcd.mpderror;

import libmpcd.c.error;

/**
 * MPD's error codes.
 */
enum MPDErrorCode
{
    /** no error */
    Success = mpd_error.MPD_ERROR_SUCCESS,
    /** out of memory */
    OOM = mpd_error.MPD_ERROR_OOM,
    /** a function was called with an unrecognized or invalid argument */
    Argument = mpd_error.MPD_ERROR_ARGUMENT,
    /** a function was called which is not available in the current state
     *  of libmpdclient */
    State = mpd_error.MPD_ERROR_STATE,
    /** timeout trying to talk to MPD */
    Timeout = mpd_error.MPD_ERROR_TIMEOUT,
    /** system error */
    System = mpd_error.MPD_ERROR_SYSTEM,
    /** unknown host */
    Resolver = mpd_error.MPD_ERROR_RESOLVER,
    /** malformed response received from MPD */
    Malformed = mpd_error.MPD_ERROR_MALFORMED,
    /** connection closed by MPD */
    Closed = mpd_error.MPD_ERROR_CLOSED,
    /** The server has returned an error code, which can be queried
     * with mpd_connection_get_server_error() */
    Server = mpd_error.MPD_ERROR_SERVER,
}

/**
 * A class to represent exceptions should be thrown when any MPD error occurs.
 */
class MPDException : Exception
{
    private MPDErrorCode m_code;

    /**
     * A constructor to create a new instance of MPDException
     *
     * Params:
     *      code = MPD error code
     *      msg = a message describing the error
     *      file = the file name of the source code corresponding with where
     *          the error was thrown from
     *      line = the line of the source code corresponding with where
     *          the error was thrown from
     *      next = a reference to the next error in the list
     */
    pure nothrow @nogc @safe this(MPDErrorCode code, string msg,
        string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        this.m_code = code;
        super(msg, file, line, next);
    }

    /**
     * Returns:
     *      MPD error code
     */
    public MPDErrorCode code()
    {
        return this.m_code;
    }
}
