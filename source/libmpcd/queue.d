module libmpcd.queue;

/**
 * A structure that represents the current playlist.
 */
struct Queue
{
    import libmpcd.core.connection;
    import libmpcd.entities;

    private Connection connection;

    /**
     * Lists all songs in the queue including meta information.
     *
     * Returns:
     *      An array of recieved songs.
     */
    Song[] list()
    {
        import libmpcd.c.queue;
        import libmpcd.c.entity;

        Song[] result;

        auto success = mpd_send_list_queue_meta(this.connection.context);
        if (!success)
            this.connection.expectError();

        for (;;)
        {
            auto current = mpd_recv_entity(this.connection.context);

            if (current is null)
            {
                this.connection.checkError();
                break; // end of entity stream
            }
            else
                result ~= wrapEntity(current).get!Song();
        }
        return result;
    }
    /**
     * Removes a song from the queue.
     *
     * Params:
     *      position = the position of the song to be removed
     */
    void remove(uint position)
    {
        import libmpcd.c.queue;
        auto success = mpd_run_delete(this.connection.context, position);
        if (!success)
            this.connection.expectError();
    }

    /**
     * Removes songs from the queue.
     *
     * Params:
     *      start = the first position of the range to be removed (including)
     *      end = the last position of the range (excluding); end equals to
     *      uint.max makes the end of the range open
     */
    void remove(uint start, uint end)
    {
        import libmpcd.c.queue;
        auto success = mpd_run_delete_range(this.connection.context,
            start, end);
        if (!success)
            this.connection.expectError();
    }

    /**
     * Removes all songs from the queue.
     */
    void remove()
    {
        import libmpcd.c.queue;
        auto success = mpd_run_clear(this.connection.context);
        if (!success)
            this.connection.expectError();
    }

    /**
     * Adds a song to the queue.
     *
     * Params:
     *      uri = the URI of the song to be added
     */
    void add(const char[] uri)
    {
        import libmpcd.c.queue;
        import std.string;

        auto success = mpd_run_add(this.connection.context, toStringz(uri));
        if (!success)
            this.connection.expectError();
    }

    /**
     * Shuffles the queue.
     */
    void shuffle()
    {
        import libmpcd.c.queue;

        auto success = mpd_run_shuffle(this.connection.context);
        if (!success)
            this.connection.expectError();
    }
}
