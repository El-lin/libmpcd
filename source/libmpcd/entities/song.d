module libmpcd.entities.song;

import libmpcd.c.tag : mpd_tag_type;

/**
 * Tags that represent metadata for a song.
 */
enum TagType
{
    Unknown = mpd_tag_type.MPD_TAG_UNKNOWN,

    Artist = mpd_tag_type.MPD_TAG_ARTIST,
    Album = mpd_tag_type.MPD_TAG_ALBUM,
    AlbumArtist = mpd_tag_type.MPD_TAG_ALBUM_ARTIST,
    Title = mpd_tag_type.MPD_TAG_TITLE,
    Track = mpd_tag_type.MPD_TAG_TRACK,
    Name = mpd_tag_type.MPD_TAG_NAME,
    Genre = mpd_tag_type.MPD_TAG_GENRE,
    Date = mpd_tag_type.MPD_TAG_DATE,
    Composer = mpd_tag_type.MPD_TAG_COMPOSER,
    Performer = mpd_tag_type.MPD_TAG_PERFORMER,
    Comment = mpd_tag_type.MPD_TAG_COMMENT,
    Disc =  mpd_tag_type.MPD_TAG_DISC,

    MpdTagCount = mpd_tag_type.MPD_TAG_COUNT,
}

/**
 * Special Musicbrainz tags for a song.
 */
enum Musicbrainz
{
    ArtistID = mpd_tag_type.MPD_TAG_MUSICBRAINZ_ARTISTID,
    AlbumID = mpd_tag_type.MPD_TAG_MUSICBRAINZ_ALBUMID,
    AlbumArtistID = mpd_tag_type.MPD_TAG_MUSICBRAINZ_ALBUMARTISTID,
    AlbumTrackID = mpd_tag_type.MPD_TAG_MUSICBRAINZ_TRACKID,
    ReleaseTrackID = mpd_tag_type.MPD_TAG_MUSICBRAINZ_RELEASETRACKID,
}

/**
 * Structure that represents an abstraction over the current playing song.
 */
struct Song
{
    import std.string;
    import libmpcd.c.song;

    private const(mpd_song)* context;

    /**
     * Returns:
     *      song URI; this is either a path relative to the MPD music directory
     *      (without leading slash), or an URL with a scheme
     */
    auto uri()
    {
        return fromStringz(mpd_song_get_uri(this.context));
    }

    /**
    * Returns:
    *       song duration in seconds, 0 means the duration is unknown.
    */
    auto duration()
    {
        return mpd_song_get_duration(this.context);
    }

    /**
     * Returns:
     *      artist
     */
    auto artist()
    {
        return fromStringz(mpd_song_get_tag(this.context, TagType.Artist, 0));
    }

    /**
     * Returns:
     *      song title
     */
    auto title()
    {
        return fromStringz(mpd_song_get_tag(this.context, TagType.Title, 0));
    }

    /**
     * Returns:
     *      track number
     */
    auto trackNumber()
    {
        return fromStringz(mpd_song_get_tag(this.context, TagType.Track, 0));
    }
}
