module libmpcd.entities.playlist;

/**
 * Structure that represents a stored playlist.
 */
struct Playlist
{
    import libmpcd.c.opaque_types;
    import libmpcd.c.playlist;
    import std.string;

    private const(mpd_playlist)* context;

    /**
     * Returns the path name of this playlist file. It does not begin
     * with a slash.
     */
    auto path()
    {
        return fromStringz(mpd_playlist_get_path(context));
    }
}
