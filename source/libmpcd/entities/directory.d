module libmpcd.entities.directory;

/**
 * Structure that represents the directory in database.
 */
struct Directory
{
    import libmpcd.c.opaque_types;
    import libmpcd.c.directory;
    import std.string;

    private const(mpd_directory)* context;

    /**
     * Returns the path of this directory, relative to the MPD music
     * directory. It does not begin with a slash.
     */
    auto path()
    {
        return fromStringz(mpd_directory_get_path(this.context));
    }
}
