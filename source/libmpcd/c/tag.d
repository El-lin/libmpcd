/* libmpdclient
   (c) 2003-2015 The Music Player Daemon Project
   This project's homepage is: http://www.musicpd.org

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   - Neither the name of the Music Player Daemon nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

module libmpcd.c.tag;

extern (C):

enum mpd_tag_type
{
    /**
    	 * Special value returned by mpd_tag_name_parse() when an
    	 * unknown name was passed.
    	 */
    MPD_TAG_UNKNOWN = -1,

    MPD_TAG_ARTIST = 0,
    MPD_TAG_ALBUM = 1,
    MPD_TAG_ALBUM_ARTIST = 2,
    MPD_TAG_TITLE = 3,
    MPD_TAG_TRACK = 4,
    MPD_TAG_NAME = 5,
    MPD_TAG_GENRE = 6,
    MPD_TAG_DATE = 7,
    MPD_TAG_COMPOSER = 8,
    MPD_TAG_PERFORMER = 9,
    MPD_TAG_COMMENT = 10,
    MPD_TAG_DISC = 11,

    MPD_TAG_MUSICBRAINZ_ARTISTID = 12,
    MPD_TAG_MUSICBRAINZ_ALBUMID = 13,
    MPD_TAG_MUSICBRAINZ_ALBUMARTISTID = 14,
    MPD_TAG_MUSICBRAINZ_TRACKID = 15,
    MPD_TAG_MUSICBRAINZ_RELEASETRACKID = 16,

    MPD_TAG_COUNT = 17
}

/**
 * Looks up the name of the specified tag.
 *
 * @return the name, or NULL if the tag type is not valid
 */
const(char)* mpd_tag_name (mpd_tag_type type);

/**
 * Parses a tag name, and returns its #mpd_tag_type value.
 *
 * @return a #mpd_tag_type value, or MPD_TAG_UNKNOWN if the name was
 * not recognized
 */
mpd_tag_type mpd_tag_name_parse (const(char)* name);

/**
 * Same as mpd_tag_name_parse(), but ignores case.
 *
 * @return a #mpd_tag_type value, or MPD_TAG_UNKNOWN if the name was
 * not recognized
 */
mpd_tag_type mpd_tag_name_iparse (const(char)* name);

