/* libmpdclient
   (c) 2003-2015 The Music Player Daemon Project
   This project's homepage is: http://www.musicpd.org

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

   - Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.

   - Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

   - Neither the name of the Music Player Daemon nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*! \file
 * \brief MPD client library
 *
 * Do not include this header directly.  Use mpd/client.h instead.
 */

module libmpcd.c.output;

import libmpcd.c.opaque_types;
import libmpcd.c.pair;

extern (C):

/**
 * Begins parsing a new #mpd_output.
 *
 * @param pair the first pair in this output (name is "outputid")
 * @return the new #mpd_output object, or NULL on error (out of
 * memory, or wrong pair name)
 */
mpd_output* mpd_output_begin (const(mpd_pair)* pair);

/**
 * Parses the pair, adding its information to the specified
 * #mpd_output object.
 *
 * @return true if the pair was parsed and added to the output (or if
 * the pair was not understood and ignored), false if this pair is the
 * beginning of the next output
 */
bool mpd_output_feed (mpd_output* output, const(mpd_pair)* pair);

/**
 * Frees a mpd_output object returned from mpd_recv_output().
 */
void mpd_output_free (mpd_output* output);

/**
 * @return the id of the specified #mpd_output object
 */
uint mpd_output_get_id (const(mpd_output)* output);

/**
 * @return the configured name of the specified #mpd_output object
 */
const(char)* mpd_output_get_name (const(mpd_output)* output);

/**
 * @return true if this output is enabled
 */
bool mpd_output_get_enabled (const(mpd_output)* output);

/**
 * Sends the "outputs" command to MPD.  Call mpd_recv_output() to
 * read the response.
 *
 * @param connection A valid and connected mpd_connection.
 * @return true on success
 */
bool mpd_send_outputs (mpd_connection* connection);

/**
 * Reads the next mpd_output from the MPD response.  Free the return
 * value with mpd_output_free().
 *
 * @return a mpd_output object on success, NULL on error or
 * end-of-response
 */
mpd_output* mpd_recv_output (mpd_connection* connection);

/**
 * Sends the "enableoutput" command to MPD.
 *
 * @param connection A valid and connected mpd_connection.
 * @param output_id an identifier for the output device (see
 * mpd_recv_output())
 * @return true on success
 */
bool mpd_send_enable_output (mpd_connection* connection, uint output_id);

/**
 * Shortcut for mpd_send_enable_output() and mpd_response_finish().
 *
 * @param connection A valid and connected mpd_connection.
 * @param output_id an identifier for the output device (see
 * mpd_recv_output())
 * @return true on success
 */
bool mpd_run_enable_output (mpd_connection* connection, uint output_id);

/**
 * Sends the "disableoutput" command to MPD.
 *
 * @param connection A valid and connected mpd_connection.
 * @param output_id an identifier for the output device (see
 * mpd_recv_output())
 * @return true on success
 */
bool mpd_send_disable_output (mpd_connection* connection, uint output_id);

/**
 * Shortcut for mpd_send_disable_output() and mpd_response_finish().
 *
 * @param connection A valid and connected mpd_connection.
 * @param output_id an identifier for the output device (see
 * mpd_recv_output())
 * @return true on success
 */
bool mpd_run_disable_output (mpd_connection* connection, uint output_id);

/**
 * Sends the "toggleoutput" command to MPD.
 *
 * @param connection a valid and connected mpd_connection.
 * @param output_id an identifier for the output device (see
 * mpd_recv_output())
 * @return true on success
 */
bool mpd_send_toggle_output (mpd_connection* connection, uint output_id);

/**
 * Shortcut for mpd_send_toggle_output() and mpd_response_finish().
 *
 * @param connection a valid and connected mpd_connection.
 * @param output_id an identifier for the output device (see
 * mpd_recv_output())
 * @return true on success
 */
bool mpd_run_toggle_output (mpd_connection* connection, uint output_id);

