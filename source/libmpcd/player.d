module libmpcd.player;

import libmpcd.c.status : mpd_state;
import libmpcd.client;

/**
 * MPD's playback state.
 */
enum PlaybackState
{
    /** no information available */
    Unknown = mpd_state.MPD_STATE_UNKNOWN,
    /** not playing */
    Stop    = mpd_state.MPD_STATE_STOP,
    /** playing */
    Play    = mpd_state.MPD_STATE_PLAY,
    /** playing, but paused */
    Pause   = mpd_state.MPD_STATE_PAUSE,
}

/**
 * A structure that represents an abstraction over the playback functionality
 * of MPD.
 */
struct Player
{
    import libmpcd.core.connection;
    import libmpcd.entities.song;

    private Connection connection;

    /**
     * Playback state of the player (play/stop/pause).
     *
     * Returns:
     *      the state of the player
     */
    PlaybackState state()
    {
        import libmpcd.c.status : mpd_status_get_state, mpd_run_status;

        auto status = mpd_run_status(this.connection.context);
        if (status is null)
            this.connection.expectError();
        return cast(PlaybackState) mpd_status_get_state(status);
    }

    /**
     * Finds out what the current playing song is.
     *
     * Returns:
     *      metadata for current song
     */
    Song currentSong()
    {
        import libmpcd.c.player : mpd_run_current_song;
        import libmpcd.core.exception;

        auto song = mpd_run_current_song(this.connection.context);
        if (song is null)
            throw new APIException("Currently no song is playing");
        this.connection.checkError();
        return Song(song);
    }

    /**
     * Starts playing the current song from the beginning.
     */
    void play()
    {
        import libmpcd.c.player;

        auto success = mpd_run_play(this.connection.context);
        if (!success)
            this.connection.expectError();
    }

    /**
     * Stops playing the current song.
     */
    void stop()
    {
        import libmpcd.c.player;

        auto success = mpd_run_stop(this.connection.context);
        if (!success)
            this.connection.expectError();
    }

    /**
     * Pauses the current song.
     */
    void togglePause()
    {
        import libmpcd.c.player;

        auto success = mpd_run_toggle_pause(this.connection.context);
        if (!success)
            this.connection.expectError();
    }

    /**
     * Starts playing the next song in playlist.
     */
    void next()
    {
        import libmpcd.c.player;

        if (this.state() != PlaybackState.Play)
            this.connection.expectError();

        auto success = mpd_run_next(this.connection.context);
        if (!success)
            this.connection.expectError();
    }
}
