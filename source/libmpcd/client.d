module libmpcd.client;
/**
 * A structure to represent an abstraction over the client to interact with
 * a MPD sever.
 */
struct Client
{
    import libmpcd.core.connection;
    import libmpcd.player;
    import libmpcd.queue;
    import libmpcd.database;

    private Connection connection;

    @disable this();

    /**
     * A constructor.
     *
     * Params:
     *       connection = a Connection instance for contacting with MPD server
     */
    private this(Connection connection)
    {
        this.connection = connection;
    }

    /**
     * Opens a new connection to a MPD server.
     *
     * Params:
     *       host = the server's host name
     *       port = the port to connect to
     *       timeout_ms = the timeout in milliseconds
     *
     * Returns:
     *       a Client instance with new connection
     */
    static Client create(const char[] host, uint port,
        uint timeout_ms = 1000)
    {
        import libmpcd.c.connection;
        import std.string;

        auto c_host = toStringz(host);
        auto context = mpd_connection_new(c_host, port, timeout_ms);
        return Client(Connection(context).checkError());
    }

    /**
     * Provides access to playback control and status.
     *
     * Returns:
     *      struct that provides access to playback control and status
     */
    Player player()
    {
        return Player(this.connection);
    }

    /**
     * Returns:
     *      struct that provides access to current queue
     */
    Queue queue()
    {
        return Queue(this.connection);
    }

    /**
     * Returns:
     *      struct that provides access to MPD music database (library)
     */
    Database database()
    {
        return Database(this.connection);
    }
}
